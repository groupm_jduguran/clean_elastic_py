#!/usr/bin/env python


"""
clean_elastic.py
This application will match post ids from elasticsearch to mysql, remove if does not exists
"""

import pymysql
from hbconfig import Config
import argparse
import pandas as pd
from sqlalchemy import engine, create_engine
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan as escan, bulk
import sys, json, time

es = None
inca_cred = None
my_cred = None
myinca_cred = None
elastic_cred = None
engine = None
es_ids = []

def process():
    global engine
    print('[INFO] Start processing script')
    all_start = time.process_time()

    read_db_credentials()  
    connect_mysql()
    init_es()
    
    df  = get_post_ids() 
    print("[INFO] Total DB data count : " , df.count())  

    df2 = get_index_partner_data()   
    print("[INFO] total elastic : " , df2.count())

    #merge sql and es result
    all_list = df2.merge(df, left_on="id", right_on="mids", how='left') 

    to_delete = all_list[all_list['mids'].isnull()]['id'] 
    
    print("[INFO] Total items to delete", to_delete.count()) 
    print("[INFO] Processing delete...") 
    delete_es_ids(to_delete) 

    print("[INFO] Overall Process Time: ", time.process_time() - all_start)
    
def read_db_credentials():

    global inca_cred, my_cred, myinca_cred, elastic_cred

    print("[INFO] Reading credentials")

    f=open(args.config, "r")
    if f.mode == 'r':
        contents=f.read()
        inca_cred = json.loads(contents)
    f.close()

    my_cred = dict()
    my_cred = inca_cred['mysql']['credentials'].get(str(args.locale))
    my_cred['host'] = inca_cred['mysql'][env]['host']
    my_cred['port'] = inca_cred['mysql'][env]['port'] 

    # myinca_cred = dict()
    # myinca_cred = inca_cred['mysql-incatech']['credentials'].get(str(args.locale))
    # myinca_cred['host'] = inca_cred['mysql-incatech'][env]['host']
    # myinca_cred['port'] = inca_cred['mysql-incatech'][env]['port']

    elastic_cred = dict()
    elastic_cred = inca_cred['elasticsearch'][env]
    
def connect_mysql():
    global my_cred, engine

    print("[INFO] Connecting to MySQL")  
    engine = create_engine(f"mysql+pymysql://{my_cred['username']}{args.worker}:{my_cred['password']}@{my_cred['host']}:{my_cred['port']}/{my_cred['db']}") 
 
    engine.connect()

def get_post_ids():
    global engine
    
    start = time.process_time()

    print("[INFO] Pulling records from table mcontent__partner_asset_scan_post")

    qry = "SELECT id as mids from mcontent__partner_asset_scan_post"

    df = pd.read_sql_query(qry, engine)

    print("[INFO] Elapse Time: ", time.process_time() - start)

    return df

#delete ids from elasticsearch
def delete_es_ids(ids, step=None): 
        def _get_bulk(_id): 
            print("[INFO] Deleting index:  ", _id)  
            doc = {
                '_op_type': 'delete',
                "_index": str(args.index_prefix)+"_"+str(args.locale) if str(args.index_prefix) != "" else str(args.locale),
                "_type": "_doc",
                "_id": _id
            }
            return doc
        actions = (_get_bulk(_id) for _id in ids)
        return bulk(es, actions, chunk_size=500, stats_only=True, raise_on_error=False) 

# Process hits
def process_hits(data):  
    es_ids.append(int(data['_source']['id'],0)) 
        
def get_index_partner_data():
    global engine, es
    
    start = time.process_time()
    print("[INFO] Pulling records from elastic ") 
    
    results = escan(
            es,
            index=str(args.index_prefix)+"_"+str(args.locale) if str(args.index_prefix) != "" else str(args.locale),
            doc_type="_doc",
            query={ "_source": {  "includes": [ "id"] }},
            size=10000
        )
 
    for dta in results: 
        process_hits(dta)     

    print("[INFO] Elapse Time: ", time.process_time() - start)

    return pd.DataFrame({'id' : es_ids})


def init_es():
    global es, elastic_cred

    es = Elasticsearch(elastic_cred, timeout=30, max_retries=10, retry_on_timeout=True)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--worker', type=str, required=True, help='worker to use')
    parser.add_argument('--locale', type=str, required=True, help='locale to process')
    parser.add_argument('--config', type=str, required=True, help='config file in json format') 
    parser.add_argument('--prod', dest='is_prod', action='store_true', help='boolean, set to true if prod')
    parser.add_argument('--index_prefix', type=str, required=False, help='index prefix (incatech | mcontent), default to incatech')
    parser.set_defaults(is_prod=False, index_prefix="incatech")
    args = parser.parse_args()
    
    # set environment
    if args.is_prod:
        env = "prod"
    else:
        env = "local"

    process()